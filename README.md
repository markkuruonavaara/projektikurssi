# Ohjelmiston tuotantoversion toteutusprojekti

## Vaatimukset
- Verkkopalvelun toteutussuunnitelma ketterän kehitysmallin mukaisesti
- Kukin opiskelija toimii työryhmässä tai kehitystiimin jäsenenä
- Ohjelmiston toteuttaminen julkaistavaan tuotantoversioon asti
- Ohjelmiston testaaminen: testitapauksia ja testausraportti/raportteja
- Ohjelmiston tietoturvan arviointi
- Ohjelmiston dokumentointi: verkkopalvelun sisältöä ja toimintaa kuvaavat kaaviot ja dokumentit
- Lopputuloksena on tuotantotasoinen versio julkaistuna johonkin ympäristöön, jossa sitä voi kokeilla.

## Kehitettävä palvelu

Projektin aiheen saa valita itse kunhan järjestelmässä on tietokanta, palvelin ja käyttöliittymä.

Jos haluaa, voi käyttää tunneilla kehiteltyä palvelua ja tuotteistaa, testata sekä dokumentoida sen.

## Aikataulu

Ryhmät suunnittelevat ja aikatauluttavat työnsä itse. Työn on oltava valmis suunnitellun jakson loppuun mennessä, eikä ole pahaksi, jos valmista tulee jo aiemminkin, sillä ei ole ennennäkemätöntä, että ohjelmistoprojektille joudutaan ottamaan lisäaikaa.

Projektin suunnittelu ja seuranta tehdään ketteriä projektinhallintamenetelmiä käyttäen. Ketterän kehitysmallin mukaisia välituloksia on synnyttävä projektin aikana. 

## Dokumentointi
Dokumentaatio suositellaan tehtäväksi Markdown-dokumentteina projektin repositoryyn (ks. [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)).  

Oheisia dokumenttipohjia voi käyttää apuna työssä. Ne ovat dokumenttirunkoja,niitä voi vapaasti muotoilla. Pohjissa olevat tekstit ovat osin opastusta työn tekemiseen, ne on tarkoitus poistaa. 

- [Projektin dokumentaation malli](projekti.md)
- [Testidokumentin malli](testaus.md). Tämä dokumenttimalli yhdistää testaussession sisällön suunnittelun ja suorituksen dokumentoinnin. Suunnitelmia voi olla useita.

Dokumentaatioon voidaan liittää muita dokumentteja eri muodoissa kuten esim. web-sivuna esitettävä API-dokumentaatio tai mockup-työkalulla käytettettävä UI-mockup. Dokumentin ei siis tarvitse olla perinteisessä mielessä "dokumentti", kunhan se on jollakin tavoin toimitettavissa asiakkaalle. 

## Palautus

Tee projektillesi git-repository ja palauta linkki siihen. Repositoryn tulee sisältää:

- Lähdekoodit
- Dokumentaatio
- Testidokumentaatio (vähintään yksi suunnitelma ja sen suorituksen muistiinpanot)
- Projektin suunnittelun ja seurannan dokumentaatio, esim. linkki scrum-boardille
-Linkki toimivaan järjestelmään sekä tunnistetiedot, joilla sitä pääsee käyttämään.

Valmistaudu myös demoamaan järjestelmääsi.